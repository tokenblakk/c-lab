#include <iostream>
#include <conio.h>
using namespace std;

//Main
void main()
{
	
	void DisplayBanner();
	int ComputeArea();
	DisplayBanner();
	ComputeArea();
}

void DisplayBanner()
{
	system("cls"); //Clear Screen

	cout << "\n\n"
		<< " PROGRAM:   Lab 2 \n\n"
		<< " AUTHOR:    Tramel Jones \n"
		<< " DATE:      09/22/13 \n"
		<< " COMPILER:  C++ using VS2012 \n"
		<< " COMPUTER:  AMD 440 \"NINJA\" \n"
		<< " VERSION:   V1.0 \n";
		
		cout << " Purpose:   To compute the area given the length and width" << endl;

	cout <<  "\n\n\t\t Press <Enter> to continue" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
}

int ComputeArea()
//This function computes the area given the length and width
{
	int length, width, area;
	length = 20;
	width = 16;
	area = width * length; //Compute area
	system("cls"); //Clear Screen

	cout << "If length = " << length << " & width = " 
		<< width << " area  = " << area;
	//Display output.
	cout <<  "\n\n\t\t Press <Enter> to continue\t" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
	return 0;
}