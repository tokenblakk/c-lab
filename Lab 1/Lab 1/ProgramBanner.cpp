#include <iostream>
#include <conio.h>
using namespace std;

//Main
void main()
{
	void DisplayBanner();
	DisplayBanner();
}

void DisplayBanner()
{
	system("cls"); //Clear Screen

	cout << "\n\n"
		<< " PROGRAM:   Lab 1 \n\n"
		<< " AUTHOR:    Tramel Jones \n"
		<< " DATE:      09/22/13 \n"
		<< " COMPILER:  C++ using VS2012 \n"
		<< " COMPUTER:  AMD 440 \"NINJA\" \n"
		<< " VERSION:   V1.0 \n";
		
		cout << " Purpose:   To display the program banner" << endl;

	cout <<  "\n\n\t\t Press <Enter> to continue" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
}