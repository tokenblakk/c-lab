#include <iostream>
#include <conio.h>
using namespace std;

//Main
void main()
{
	void DisplayBanner();
	int DemoMathOperators();

	DisplayBanner();
	DemoMathOperators();
}

void DisplayBanner()
{
	system("cls"); //Clear Screen

	cout << "\n\n"
		<< " PROGRAM:   Lab 3-A1 \n\n"
		<< " AUTHOR:    Tramel Jones \n"
		<< " DATE:      09/22/13 \n"
		<< " COMPILER:  C++ using VS2012 \n"
		<< " COMPUTER:  AMD 440 \"NINJA\" \n"
		<< " VERSION:   V1.0 \n";
		
		cout << " Purpose:   Areitmetic Operators" << endl;

	cout <<  "\n\n\t\t Press <Enter> to continue" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
}

int DemoMathOperators()
{
	float num1, num2, sum, difference, product, division;
	int num3, num4, quotient, remainder;

	cout << "Enter two float numbers num1 and num2 separated by a space:";
	// use the following test sets    {15.0 3.0}, {14.0 3.0}
	cin >> num1 >> num2;

	cout << "Enter two integers num3 and num4 separated by a space:";
	cin >> num3 >> num4;
	
	sum = (num1 + num2);
	difference = (num1 - num2);
	product = num1 * num2;
	division = num1 / num2;
	quotient = num3 / num4;
	remainder = num3 % num4;

	   cout << "You entered:  " << "num1  =  " << num1 <<  " and  "<<  "num2 =  "<<  num2 << endl;
       cout << "You entered:  " << "num3  =  " << num3 <<  " and  "<<  "num4 =  "<<  num4 << endl;

	   cout << num1 << " + " << num2 << " = "<< sum << endl;

	   cout << num1 << " - " << num2 << " = " << difference << endl;

	   cout << num1 << " * " << num2 << " = " << product << endl;

	   cout << num1 << " / " << num2 << " = " << division << endl;

	   cout << num3 << " / " << num4 << " = " << quotient << endl;

	   cout << num3 << " % " << num4 << " = " << remainder << endl;
	   
	cout <<  "\n\n\t\t Press <Enter> to continue" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
	   return 0;

}