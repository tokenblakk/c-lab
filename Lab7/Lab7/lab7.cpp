#include <iostream>
#include <conio.h>
#include <vector>
#include <sstream>
using namespace std;

void PromptUserForSelection();
void DisplayBanner();
void ServiceSelectionUsingIfElse(string input);
float ExecuteWhileLoop(int count);
float ExecuteDoWhileLoop(int count);
float ExecuteForLoop(int count);
char DetermineLtrGrdUsingSwitch(int grade);
void DisplayStudentRecord(float avg, char ltrGrd);


void main()
{
	DisplayBanner();
	PromptUserForSelection();
}
void DisplayBanner()
{
	system("cls"); //Clear Screen

	cout << "\n\n"
		<< " PROGRAM:   Lab 7 \n\n"
		<< " AUTHOR:    Tramel Jones \n"
		<< " DATE:      12/5/13 \n"
		<< " COMPILER:  C++ using VS2013 \n"
		<< " COMPUTER:  Intel i7 3770k\"GLaDOS\" \n"
		<< " VERSION:   V1.0 \n";

	cout << " Purpose: Teacher Menu Using CallByValue Functions " << endl;

	cout << "\n\n\t\t Press <Enter> to continue" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
}
void PromptUserForSelection()
{
	bool quit = false;
	do
	{
		system("cls");//Clear
		cout << "Welcome." << endl << "Please enter a loop structure" << endl << endl << "STRUCTURE       SELECTION"
			<< endl << "While: W" << endl << "DO While: D" << endl << "For: F" << endl << endl << "Enter Selection <Q to Quit>:";
		//_getch();
		switch (_getch())
		{
		case 81:
			quit = true;
			break;
		case 113:
			quit = true;
			break;

		case 87: //W - While
			;
		case 119:
			ServiceSelectionUsingIfElse("W");
			break;


		case 68://D - Do While
			;
		case 100:
			ServiceSelectionUsingIfElse("D");
			break;

		case 70: //F - For
			;
		case 102:
			ServiceSelectionUsingIfElse("F");
			break;

		}
	} while (quit == false);//waits for user input to exit. Q or q
	cout << "\nYou Selected to Quit.  BYE" << endl;
	_getch();
}

void ServiceSelectionUsingIfElse(string input)
{
	int grades = 0;
	float avg;
	char ltrGrd;
	cout << "\nPlease enter the number of grades that will be entered for the student: ";
	cin >> grades;
	
	//record [grades]; //set length of array to grade entries.


	if (input == "W")
	{
		cout << "\nYou selected the <While> Structure" << endl;
		avg = ExecuteWhileLoop(grades);
	}
	else if (input == "D")
	{
		cout << "\nYou selected the <Do While> Structure" << endl;
		avg = ExecuteDoWhileLoop(grades);

	}
	else if (input == "F")
	{
		cout << "\nYou selected the <For> Structure" << endl;
		avg = ExecuteForLoop(grades);
	}

	ltrGrd = DetermineLtrGrdUsingSwitch((int)avg);
	DisplayStudentRecord(avg, ltrGrd);
}

float ExecuteWhileLoop(int count)
{
	vector <int>record;
	int i = 0;
	int score;
	float avg;
	while (i < count)
	{
		cout << "Enter Score number " << i + 1 << ":     " << endl;
		cin >> score;
		if (score >100 || score < 0)
			;
		else
		{
			record.push_back(score);
			i++;
		}
	}

	int total = 0;

	for (int j = 0; j < count; j++)
	{
		total = total + record[j];
	}
	avg = (float)(total / count);
	cout << "\n Average Score: " << avg;
	_getch();
	return avg;
}
float ExecuteDoWhileLoop(int count)
{
	vector <int>record;
	int i = 0;
	int score;
	float avg;
	do
	{
		cout << "Enter Score number " << i + 1 << ":     " << endl;
		cin >> score;
		if (score > 100 || score < 0)
			;
		else
		{
			record.push_back(score);
			i++;
		}
	} while (i < count);

	int total = 0;
	;

	for (int j = 0; j < count; j++)
	{
		total = total + record[j];
	}
	avg = (float)(total / count);
	cout << "\n Average Score: " << avg;
	_getch();
	return avg;
}
float ExecuteForLoop(int count)
{
	vector <int>record;
	int score;
	float avg;
	for (int i = 0; i< count; i++)
	{
		cout << "Enter Score number " << i + 1 << ":     " << endl;
		cin >> score;
		if (score > 100 || score < 0)
			i--;
		else
		{
			record.push_back(score);
		}
	}

	int total = 0;


	for (int j = 0; j < count; j++)
	{
		total = total + record[j];
	}
	avg = (float)(total / count);
	cout << "\n Average Score: " << avg;
	_getch();
	return avg;
}

char DetermineLtrGrdUsingSwitch(int grade)
{
	char ltrGrd ='z';
	switch (grade) //switch takes only constants not expressions such as < = >
	{
	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
	case 9:
	case 10:
	case 11:
	case 12:
	case 13:
	case 14:
	case 15:
	case 16:
	case 17:
	case 18:
	case 19:

	case 20:
	case 21:
	case 22:
	case 23:
	case 24:
	case 25:
	case 26:
	case 27:
	case 28:
	case 29:

	case 30:
	case 31:
	case 32:
	case 33:
	case 34:
	case 35:
	case 36:
	case 37:
	case 38:
	case 39:

	case 40:
	case 41:
	case 42:
	case 43:
	case 44:
	case 45:
	case 46:
	case 47:
	case 48:
	case 49:

	case 50:
	case 51:
	case 52:
	case 53:
	case 54:
	case 55:
	case 56:
	case 57:
	case 58:
	case 59:

	case 60: ltrGrd = 'F'; break;
	case 61:
	case 62:
	case 63:
	case 64:
	case 65:
	case 66:
	case 67:
	case 68:
	case 69: ltrGrd = 'D'; break;

	case 70:
	case 71:
	case 72:
	case 73:
	case 74:
	case 75:
	case 76:
	case 77:
	case 78:
	case 79: ltrGrd = 'C'; break;

	case 80:
	case 81:
	case 82:
	case 83:
	case 84:
	case 85:
	case 86:
	case 87:
	case 88:
	case 89: ltrGrd = 'B'; break;

	case 90:
	case 91:
	case 92:
	case 93:
	case 94:
	case 95:
	case 96:
	case 97:
	case 98:
	case 99:
	case 100: ltrGrd = 'A'; break;
	default:
		break;
	}
	return ltrGrd;
}

void DisplayStudentRecord(float avg_score, char letter_grade)
{
	cout << "Please enter your first and last name: ";
	//scanf_s("%s", &name);
	//causes terrible crash

	system("cls");
	cout << "Student Record:" << endl << "     Student Name:     " /*<< name*/ << endl;
	cout << "     Grade Average:     " << avg_score << endl;
	cout << "     Letter Grade:     " << letter_grade << endl;


	cout << "\n\n\t\t Press <Enter> to continue" << endl;
	while (_getch() != 13) //waits for user input to exit.
	{
		_getch();
	}
}